# Intel i915-dkms open source driver with patch

Please read carefully, this Intel drivers is "semi" propietary cause some of the license and some of the additional rights.

## Disclaimer
I just patching this drivers to work in GNU/Linux OS, all risk or broken on your machine is your responsibility!

DO IT YOUR OWN RISK!

## License
The Intel® i915-dkms for Linux* OS is made available under the terms of the Apache Software License 2.0.

## Tested on
Ubuntu GNU/Linux 16.04.4 LTS using the latest kernel that enabled retpolines compiler

## Installation
1. Download or clone this repo.
2. Open terminal and change directory to `i915-dkms(-master)`
3. Patch this driver using this command `patch -p1 -i intel_audio.patch`.
4. Place the folder of `i915-4.6.3-4.4.0-1` to `/usr/src/`
5. Build using DKMS with this command => `sudo dkms build i915-4.6.3-4.4.0/1`.
6. Install the driver using DKMS with this  command => `sudo dkms install i915-4.6.3-4.4.0/1`.
7. Reboot to see the change of your machine.

## Changelogs
Please read the *.patch files

## Credits
1. All Intel or other devs who made this driver
2. <a href="https://github.com/kafabih-kr"> Kafabih (Me)</a>
